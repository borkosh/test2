﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2
{
    class Program
    {
        static void getThreeNumbers(int[] a, int sum)
        {
            int length = a.Length;
            Array.Sort(a);

            for (int i = 0; i < length - 1; i++)
            {
                int left = i + 1;
                int right = length - 1;
                while (left < right)
                {
                    if (a[i] + a[left] + a[right] == sum)
                    {
                        Console.WriteLine(a[i] + " " + a[left] + " " + a[right]);
                        left++;
                        right--;
                    }
                    else if (a[i] + a[left] + a[right] < sum)
                        left++;
                    else
                        right--;
                }
            }
        }

        static void Main(string[] args)
        {
            int[] a = new int[] { -1, 2, 22, 213123123, 0, 23, 1 };
            int sum = 24;
            getThreeNumbers(a, sum);
            Console.ReadKey();

        }
    }
}
